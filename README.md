# Marsforming #

Just a little kidding using the beautiful [Terraform tool](https://www.terraform.io/) to create a docker instance with the Tor + privoxy configuration to enable anonymous Internet surfing.

## How to use ##

First at all, you need to download the official [terraform binary](https://www.terraform.io/downloads.html) to your plataform.

After unpacking the package and configure the `PATH` to find the binary, just clone this repo like that:

```
git clone https://gitlab.com/richizo/marsforming.git
```

And let's rule the world:

```
cd marsforming

terraform init

terraform plan

terraform apply
```

Point the address http://localhost:8118 on your [browser's proxy configuration](https://support.mozilla.org/en-US/kb/connection-settings-firefox) and 

> `ack ack ack` :alien:
> from the [Mars Attack!](https://en.wikipedia.org/wiki/Mars_Attacks!) movie

