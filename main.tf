provider "local" {}

resource "docker_container" "torxy" {
  image = "${docker_image.torxy.latest}"
  must_run = "true"
  name = "torxy"
    ports {
      internal = 9050
      external = 9050
    }
    ports {
      internal = 9051
      external = 9051
    }
    ports {
      internal = 8118
      external = 8118
    }
}

resource "docker_image" "torxy" {
  name = "dockage/tor-privoxy"
}